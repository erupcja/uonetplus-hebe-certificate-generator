import 'dart:convert';

import 'package:basic_utils/basic_utils.dart';

class _HebeCert {
  String certificate;
  String fingerprint;
  String privateKey;
}

_HebeCert generateHebeCert() {
  final keyPair = CryptoUtils.generateRSAKeyPair();
  final cert = X509Utils.generateRsaCsrPem(
      {'CN': 'APP_CERTIFICATE CA Certificate'},
      keyPair.privateKey,
      keyPair.publicKey);
  var hebe = _HebeCert();
  hebe.privateKey = CryptoUtils.encodeRSAPrivateKeyToPem(keyPair.privateKey)
      .replaceAll('-----BEGIN PRIVATE KEY-----', '')
      .replaceAll('-----END PRIVATE KEY-----', '')
      .replaceAll('\n', '');
  hebe.certificate = cert
      .replaceAll('-----BEGIN CERTIFICATE REQUEST-----', '')
      .replaceAll('-----END CERTIFICATE REQUEST-----', '')
      .replaceAllMapped(RegExp(r'\r?\n|\r'), (m) => '');
  hebe.fingerprint =
      CryptoUtils.getSha1ThumbprintFromBytes(utf8.encode(cert)).toLowerCase();
  return hebe;
}
