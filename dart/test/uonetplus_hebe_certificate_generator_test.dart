import 'package:uonetplus_hebe_certificate_generator/uonetplus_hebe_certificate_generator.dart';
import 'package:test/test.dart';

void main() {
	  var hebeCert;
    setUp(() {
	hebeCert = generateHebeCert();
    });

    test('Tests', () {
	    expect(hebeCert.certificate.runtimeType, String);
	    expect(hebeCert.fingerprint.runtimeType, String);
	    expect(hebeCert.privateKey.runtimeType, String);
    });
}
