import { expect } from 'chai';
import generateHebeCert from './index';
import 'mocha';

describe('integration', () => {
  it('returns values successfully', (done) => {
    generateHebeCert().then((result) => {
      expect(result.certificate).to.be.a('string');
      expect(result.fingerprint).to.be.a('string');
      expect(result.privateKey).to.be.a('string');
      done();
    });
  });
});
