import forge from 'node-forge';
import crypto from 'crypto';
import { addYears } from 'date-fns';
import { promisify } from 'util';

export default async function generateHebeCert() {
  const keys = await promisify(crypto.generateKeyPair)('rsa', {
    modulusLength: 2048,
  });
  const publicKey = keys.publicKey.export({ format: 'pem', type: 'spki' }).toString();
  const privateKey = keys.privateKey.export({ format: 'pem', type: 'pkcs8' }).toString();
  const cert = forge.pki.createCertificate();
  cert.publicKey = forge.pki.publicKeyFromPem(publicKey);
  cert.privateKey = forge.pki.privateKeyFromPem(privateKey);
  cert.serialNumber = '1';
  cert.validity.notBefore = new Date();
  cert.validity.notAfter = addYears(new Date(), 20);
  const attrs = [
    {
      shortName: 'CN',
      value: 'APP_CERTIFICATE CA Certificate',
    },
  ];
  cert.setSubject(attrs);
  cert.setIssuer(attrs);
  cert.sign(cert.privateKey, forge.md.sha256.create());
  // cert.sign(cert.privateKey, forge.md.sha512.create());
  const fingerprint = crypto
    .createHash('sha1')
    .update(forge.asn1.toDer(forge.pki.certificateToAsn1(cert)).getBytes().toString(), 'latin1')
    .digest()
    .toString('hex');
  return {
    fingerprint,
    certificate: forge.pki
      .certificateToPem(cert)
      .replace('-----BEGIN CERTIFICATE-----', '')
      .replace('-----END CERTIFICATE-----', '')
      .replace(/\r?\n|\r/g, '')
      .trim(),
    privateKey: privateKey
      .replace('-----BEGIN PRIVATE KEY-----', '')
      .replace('-----END PRIVATE KEY-----', '')
      .replace(/\r?\n|\r/g, '')
      .trim(),
  };
}
