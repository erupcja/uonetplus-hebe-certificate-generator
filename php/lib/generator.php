<?php

namespace Erupcja\UonetplusHebeCertificateGenerator;

require 'vendor/autoload.php';

use FG\ASN1\ASNObject;
use FG\ASN1\Exception\ParserException;

function generateHebeCert() {
    $privkey = openssl_pkey_new(array(
        "private_key_bits" => 2048,
        "private_key_type" => OPENSSL_KEYTYPE_RSA,
    ));

    $csr = openssl_csr_new(
        array('commonName' => 'APP_CERTIFICATE CA Certificate'),
        $privkey,
        array('digest_alg' => 'sha256')
    );

    $x509 = openssl_csr_sign($csr, null, $privkey, $days=365, array('digest_alg' => 'sha256'));
    openssl_x509_export($x509, $certout);
    openssl_pkey_export($privkey, $pkeyout);

    $asn = base64_decode($certout);
    try {
        ASNObject::fromBinary($asn);
    } catch (ParserException $e) {
        die($e);
    }

    $fingerprint = sha1($asn);

    return array(
        'fingerprint' => $fingerprint,
        'certificate' => preg_replace('/\r?\n|\r/g', '',
            str_replace(
                '-----BEGIN CERTIFICATE-----', '',
                str_replace('-----END CERTIFICATE-----', '', $certout)
            )
        ),
        'privateKey' => preg_replace('/\r?\n|\r/g', '',
            str_replace(
                '-----END PRIVATE KEY-----', '',
                str_replace('-----BEGIN PRIVATE KEY-----', '', $pkeyout)
            )
        ),
    );
}